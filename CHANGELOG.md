# CHANGELOG

<!--- next entry here -->

## 1.0.1
2019-05-14

### Fixes

- Removed GSG_INITIAL_DEVELOPMENT var (6e7a6e743b46579650aaeb09788ca6e9a06c7c66)
- update release version (14bedf544202ae04a2b0bfec7b19d3717c6365ad)

## 0.1.0
2019-05-13

### Features

- Added gitlab-ci and gitignore files (bfd988423ba6458b2041c6b8f1e95a900b297b5d)

### Fixes

- Removed GSG_INITIAL_DEVELOPMENT var (6e7a6e743b46579650aaeb09788ca6e9a06c7c66)

## 1.0.0
2019-05-13

### Features

- Added gitlab-ci and gitignore files (bfd988423ba6458b2041c6b8f1e95a900b297b5d)